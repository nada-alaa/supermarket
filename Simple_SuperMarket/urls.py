from django.urls import path
from django.contrib.auth import views as auth_views
from Simple_SuperMarket import views


urlpatterns = [
    path('accounts/login/', auth_views.LoginView.as_view(template_name='users/login.html', redirect_authenticated_user=True), name='login'),
    path('accounts/logout/', auth_views.LogoutView.as_view(template_name='logged_out.html'), name='logout'),
    path('home', views.getCustomers , name='home'),
    path('customer/create',views.addCustomer , name='newCustomer'),
    path('customer/view/<num>', views.showCustomer , name='showCustomer'),
    path('invoice/new',views.createInvoice , name='newInvoice'),
    path('invoice/view/<num>',views.showInvoice , name='showInvoice'),
    path('printInvoice/<num>',views.Invoice_pdf , name='PrintPdf')
]