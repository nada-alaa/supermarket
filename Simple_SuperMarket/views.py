from django.shortcuts import render , HttpResponseRedirect , HttpResponse
from Simple_SuperMarket.models import Customer , Invoice , Product
from django.contrib.auth.decorators import login_required 
from django.conf import settings 
from Simple_SuperMarket.forms import customerForm , invoiceForm
from django.template.loader import render_to_string
import weasyprint

# Create your views here.
@login_required
def getCustomers(request):
    customers=Customer.objects.all()
    context={'customers':customers}
    return render(request,'customer/main.html',context)
###############################################################################

@login_required
def addCustomer(request):
    form=customerForm()
    if request.method=="POST":
        form=customerForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect("/home")
    return render(request,'customer/new.html',{'form':form})      
##############################################################################

@login_required
def createInvoice(request):
    form=invoiceForm()
    if request.method=='POST':
        form=invoiceForm(request.POST)
        if form.is_valid():
            data = int(form['customer'].value())
            client=Customer.objects.get(id=data)
            if client.blockStatus:
                return HttpResponseRedirect("/home")
            else:
                products=form['products'].value()
                for prod in products:
                    instance=Product.objects.get(id=int(prod))
                    instance.availableQuantity-=1
                    instance.save()
                inst=form.save()
                invoice=Invoice.objects.get(id=inst.id)
                for prod in products:
                    instance=Product.objects.get(id=int(prod))
                    invoice.total+=instance.price
                    invoice.save()
                return HttpResponseRedirect("/home")
    return render(request,'invoice/new.html',{'form':form}) 

###################################################################################

@login_required
def showCustomer(request,num):
    customervar=Customer.objects.get(id=num)
    invoices=Invoice.objects.filter(customer = customervar)
    context={'customer':customervar,'invoices':invoices}
    return render(request,'customer/show.html',context) 
##########################################################################################################

@login_required
def showInvoice(request,num):
    invoice=Invoice.objects.get(id=num)
    context={'invoice':invoice}
    return render(request,'invoice/show.html',context)

##########################################################################################################

# @staff_member_required
def Invoice_pdf(request, num):
    order = Invoice.objects.get(id=num)
    html = render_to_string('invoice/orders/pdf.html',
                            {'order': order})
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'filename=\
        "invoice_{}.pdf"'.format(order.id)
    weasyprint.HTML(string=html).write_pdf(response,
        stylesheets=[weasyprint.CSS(
            settings.STATIC_ROOT + 'css/pdf.css')])
    return response