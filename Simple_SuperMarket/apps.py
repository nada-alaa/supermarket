from django.apps import AppConfig


class SimpleSupermarketConfig(AppConfig):
    name = 'Simple_SuperMarket'
