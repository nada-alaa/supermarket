from django.db import models
from phone_field import PhoneField

# # Create your models here.

class Customer(models.Model):
    name= models.CharField(max_length=50)
    phone = PhoneField(blank=True)
    email=models.EmailField(max_length=150)
    address=models.TextField(max_length=350)
    blockStatus=models.BooleanField(default=False)
    def __str__(self):
        return self.name

    def is_blocked(self):
        if self.blockStatus:
            return True
        return False

    is_blocked.boolean = True
    blockStatus.short_description = 'Blocked'

class Product(models.Model):
    name=models.CharField(max_length=200)
    price=models.FloatField()
    availableQuantity=models.PositiveIntegerField()
    def __str__(self):
        return self.name


class Invoice(models.Model):
    createdAt = models.DateTimeField(auto_now_add=True)
    shippingAddress = models.TextField(max_length=350)
    total=models.FloatField(default=0)
    customer= models.ForeignKey(Customer, on_delete= models.CASCADE) 
    products= models.ManyToManyField(Product,related_name='products')
    def __str__(self):
        return self.customer.name

