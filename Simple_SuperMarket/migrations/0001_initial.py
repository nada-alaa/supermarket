# Generated by Django 3.1.1 on 2020-09-21 18:31

from django.db import migrations, models
import django.db.models.deletion
import phone_field.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Customer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('phone', phone_field.models.PhoneField(blank=True, max_length=31)),
                ('email', models.EmailField(max_length=150)),
                ('address', models.TextField(max_length=350)),
                ('blockStatus', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
                ('price', models.FloatField()),
                ('availableQuantity', models.PositiveIntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Invoice',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('createdAt', models.DateTimeField(auto_now_add=True)),
                ('shippingAddress', models.TextField(max_length=350)),
                ('customer', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='Simple_SuperMarket.customer')),
                ('products', models.ManyToManyField(related_name='products', to='Simple_SuperMarket.Product')),
            ],
        ),
    ]
