from django.contrib import admin
from Simple_SuperMarket.models import Customer , Product , Invoice
# Register your models here.

class CustomCustomer(admin.ModelAdmin):
    list_display = ['name', 'email', 'phone', 'address', 'is_blocked']

class CustomProduct(admin.ModelAdmin):
    list_display=['name', 'availableQuantity' , 'price']

class CustomInvoice(admin.ModelAdmin):
    list_display=['customer' , 'products' , 'shippingAddress' , 'createdAt']

admin.site.register(Customer,CustomCustomer)
admin.site.register(Product,CustomProduct)
admin.site.register(Invoice)