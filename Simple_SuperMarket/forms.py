from django import forms
from Simple_SuperMarket.models import Customer , Invoice

class customerForm(forms.ModelForm):
    address=forms.CharField(label="Address",widget=forms.Textarea(attrs={'class':'form-control',  'placeholder': 'write customer\'s address here....' , 'rows':'3','cols':'50'}))
    class Meta:
        model=Customer
        fields=['name','email','address','phone']

class invoiceForm(forms.ModelForm):
    shippingAddress=forms.CharField(label="Address",widget=forms.Textarea(attrs={'class':'form-control',  'placeholder': 'write customer\'s address here....' , 'rows':'3','cols':'50'}))
    class Meta:
        model=Invoice
        fields=['customer','shippingAddress' ,'products' ]